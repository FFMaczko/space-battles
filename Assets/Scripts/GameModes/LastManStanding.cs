﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastManStanding : GameMenager {

    // Use this for initialization
    void Start()
    {
        Menager = this;
        Audio = transform.GetComponent<AudioSource>();
        UI = GameObject.Find("UI").transform.GetComponent<UIMenager>();
        bullets = GameObject.Find("Bullets").transform;
        NumberOfPlayers = GameData.Data.NumberOfPlayers;
        TargetScore = GameData.Data.TargetScore;
        Players = new PlayerControler[NumberOfPlayers];
        CreateMap();
    }

    public override void PlayerWasKilled(int killer, int killed)
    {
        int PlayersLeft = 0;
        int LastMan = -1;
        for (int i = 0; i < NumberOfPlayers; i++)
        {
            if (Players[i].IsAlive)
            {
                PlayersLeft++;
                LastMan = i;
            }
        }
        if (PlayersLeft <= 1)
        {
            if (LastMan != -1)
            {
                Players[LastMan].Points += 1;
                UI.SetPoints(LastMan, Players[LastMan].Points);
                if (Players[LastMan].Points >= TargetScore)
                {
                    EndGame(LastMan+1);
                }
                else
                {
                    StartCoroutine("WaitForExsplosion");
                }
            }
            else
            {
                StartCoroutine("WaitForExsplosion");
            }
        }
    }
}
