﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameMenager : MonoBehaviour {
    public static GameMenager Menager;
    protected Transform bullets;
    protected int NumberOfPlayers;
    protected int TargetScore;
    protected PlayerControler[] Players;
    public Vector3[] StartPositions;
    public GameObject PlayerPrefabr;
    protected UIMenager UI;
    protected AudioSource Audio;
    public AudioClip victory;

	// Use this for initialization
	void Start () {
        Menager = this;
        Audio = transform.GetComponent<AudioSource>();
        UI = GameObject.Find("UI").transform.GetComponent<UIMenager>();
        bullets = GameObject.Find("Bullets").transform;
        NumberOfPlayers = GameData.Data.NumberOfPlayers;
        TargetScore = GameData.Data.TargetScore;
        Players =new PlayerControler[NumberOfPlayers];
        CreateMap();
	}
	
    protected void CreateMap()
    {
        Time.timeScale = 0;
        CreatePlayers();
        UI.CreateScoreCounter(NumberOfPlayers);
        UI.TurnOffEndCanvas();
        StartCoroutine("CountDown");
    }

    protected void CreatePlayers()
    {
        for (int i = 0; i < NumberOfPlayers; i++)
        {
            GameObject go = Instantiate(PlayerPrefabr, transform.position, transform.rotation);
            Players[i] = go.transform.GetComponent<PlayerControler>();
            go.transform.GetComponent<PlayerControler>().PlayerNumber = i + 1;
            go.transform.GetComponent<SpriteRenderer>().sprite = GameData.Data.PlayerSprite[i];
            Players[i].MoveTo(StartPositions[i]);
        }
    }

    public abstract void PlayerWasKilled(int killer, int killed);

    protected IEnumerator WaitForExsplosion()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        RestarLevel();
    }
    protected void EndGame(int Winner)
    {
        for(int i = 0; i < NumberOfPlayers; i++)
        {
            Players[i].Mute();
        }
        Audio.PlayOneShot(victory);
        Time.timeScale = 0;
        UI.TurnONMessages();
        UI.TurnONEndCanvas();
        if (Winner != -1)
        {
            UI.SetMessage("Wygral gracz: " + Winner);
            UI.SetMessageColor(GameData.Data.PlayerColor[Winner - 1]);
        }
        else
        {
            UI.SetMessage("Remis");
        }
    }
    protected void RestarLevel()
    {
        Time.timeScale = 0;
        DestroyAllBullets();
        ResetPlayers();
        StartCoroutine("CountDown");
    }
    protected void DestroyAllBullets()
    {
        while (bullets.childCount>0)
        {
            Transform bullet = bullets.GetChild(0);
            bullet.SetParent(null);
            Destroy(bullet.gameObject);
        }
    }
    protected void ResetPlayers()
    {
        for (int i = 0; i < Players.Length; i++)
        {
            Players[i].Reset();
            Players[i].MoveTo(StartPositions[i]);
        }
    }
    protected IEnumerator CountDown()
    {
        UI.TurnONMessages();
        UI.SetMessage("3");
        yield return new WaitForSecondsRealtime(1);
        UI.SetMessage("2");
        yield return new WaitForSecondsRealtime(1);
        UI.SetMessage("1");
        yield return new WaitForSecondsRealtime(1);
        UI.SetMessage("GO!");
        yield return new WaitForSecondsRealtime(0.2f);
        UI.TurnOffMessages();
        if (!UI.GetPaused())
        { 

        Time.timeScale = 1;
        }

    }
}
