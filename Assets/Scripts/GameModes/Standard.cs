﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Standard : GameMenager {

    // Use this for initialization
    void Start()
    {
        Menager = this;
        Audio = transform.GetComponent<AudioSource>();
        UI = GameObject.Find("UI").transform.GetComponent<UIMenager>();
        bullets = GameObject.Find("Bullets").transform;
        NumberOfPlayers = GameData.Data.NumberOfPlayers;
        TargetScore = GameData.Data.TargetScore;
        Players = new PlayerControler[NumberOfPlayers];
        CreateMap();
    }

    public override void PlayerWasKilled(int killer, int killed)
    {
        int PlayersLeft = 0;
        bool GameEnded = false;
        int NumberOfWinners = 0;
        int LastWinner = -1;
        for (int i = 0; i < NumberOfPlayers; i++)
        {
            if (Players[i].IsAlive)
            {
                PlayersLeft++;
                Players[i].Points++;
                UI.SetPoints(i, Players[i].Points);
                if (Players[i].Points >= TargetScore)
                {
                    GameEnded = true;
                    NumberOfWinners += 1;
                    LastWinner = i + 1;
                }
            }
        }
        if (GameEnded)
        {
            if (NumberOfWinners == 1)
            {
                EndGame(LastWinner);
            }
            else
            {
                EndGame(-1);
            }
        }
        if (PlayersLeft <= 1 && !GameEnded)
        {
            StartCoroutine("WaitForExsplosion");
        }
    }
}
