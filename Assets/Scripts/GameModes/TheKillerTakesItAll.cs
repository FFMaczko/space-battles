﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheKillerTakesItAll : GameMenager {

    // Use this for initialization
    void Start()
    {
        Menager = this;
        Audio = transform.GetComponent<AudioSource>();
        UI = GameObject.Find("UI").transform.GetComponent<UIMenager>();
        bullets = GameObject.Find("Bullets").transform;
        NumberOfPlayers = GameData.Data.NumberOfPlayers;
        TargetScore = GameData.Data.TargetScore;
        Players = new PlayerControler[NumberOfPlayers];
        CreateMap();
    }

    public override void PlayerWasKilled(int killer, int killed)
    {
        int PlayersLeft = 0;
        bool GameEnded = false;
        int Winner = 0;

        if (killer != killed && killer!=-1)
        {
            Players[killer].Points++;
            UI.SetPoints(killer, Players[killer].Points);
            if (Players[killer].Points >= TargetScore)
            {
                GameEnded = true;
                Winner = killer + 1;
            }
        }
        else if(killer==-1)
        {
            for (int i = 0; i < NumberOfPlayers; i++)
            {
                if (i!=killed)
                {
                    Players[i].Points++;
                    UI.SetPoints(i, Players[i].Points);
                    if (Players[i].Points >= TargetScore)
                    {
                        if (!GameEnded)
                        {
                            Winner = i + 1;
                        }
                        else
                        {
                            Winner = -1;
                        }
                        GameEnded = true;
                        
                    }
                }
            }
        }



        if (GameEnded)
        {
            EndGame(Winner);
        }

        for (int i = 0; i < NumberOfPlayers; i++)
        {
            if (Players[i].IsAlive)
            {
                PlayersLeft++;
            }
        }
        if (PlayersLeft <= 1 && !GameEnded)
        {
            StartCoroutine("WaitForExsplosion");
        }
    }
}
