﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler :  MonoBehaviour , Destructible {

    public int PlayerNumber;

    //movement
    private Rigidbody2D Body;
    float EngineForce = 20;
    bool canMove=true;
    const float ROTATIONSPEED = 5;
    const float MAXSPEED = 10;

    //efekty
    //animacje
    public Animator nitro;
    public GameObject Boom;
    //dzwieki
    public AudioClip shootSound;
    public AudioSource engine;
    public AudioSource Audio;

    //shooting
    public Rigidbody2D Bullet;
    const float BULLETSPEED=12;
    const int SHOOTDELAY = 20;
    bool canShoot=true;
    Transform barrel;
    Transform bullets;

    //gamedata
    int points;
    bool isAlive=true;

    //controls
    KeyCode up;
    KeyCode right;
    KeyCode left;
    KeyCode shoot;

    public int Points
    {
        get
        {
            return points;
        }

        set
        {
            points = value;
        }
    }

    public bool IsAlive
    {
        get
        {
            return isAlive;
        }
    }

    public bool CanMove
    {
        get
        {
            return canMove;
        }

        set
        {
            canMove = value;
        }
    }

    void Start()
    {
        Body = transform.GetComponent<Rigidbody2D>();
        barrel = transform.GetChild(0);
        bullets = GameObject.Find("Bullets").transform;
        up = GameData.Data.acelerate[PlayerNumber-1];
        left = GameData.Data.left[PlayerNumber-1];
        right = GameData.Data.right[PlayerNumber-1];
        shoot = GameData.Data.shoot[PlayerNumber-1];
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 45 - 90 * (PlayerNumber-1)));
        nitro.SetBool("Flies", false);
        engine.volume = 0;
    }

    void FixedUpdate()
    {
        engine.volume = 0;
        nitro.SetBool("Flies", false);
        if (canMove)
        {

            if (Input.GetKey(right))
            {
                transform.Rotate(0, 0, -ROTATIONSPEED);
            }

            if (Input.GetKey(left))
            {
                transform.Rotate(0, 0, ROTATIONSPEED);
            }

            if (Input.GetKey(up))
            {
                Body.AddForce(transform.up * EngineForce);
                nitro.SetBool("Flies", true);
                engine.volume = 0.5f;
            }

            if (Input.GetKey(shoot))
            {
                Fire();
            }

            if (Body.velocity.magnitude > MAXSPEED)
            {
                Body.velocity = Body.velocity.normalized * MAXSPEED;
            }
        }

    }

    void Fire()
    {
        if (canShoot)
        {
            Rigidbody2D go = Instantiate(Bullet, barrel.position, transform.rotation) as Rigidbody2D;
            go.velocity = transform.up * BULLETSPEED;
            go.transform.SetParent(bullets);
            go.transform.GetComponent<SpriteRenderer>().color = GameData.Data.PlayerColor[PlayerNumber-1];
            go.transform.GetComponent<Bullet>().bulletOvner = PlayerNumber-1;
            canShoot = false;
            Audio.PlayOneShot(shootSound);
            StartCoroutine("ShootCountdown");
        }
    }

    IEnumerator ShootCountdown()
    {
        for(int i = 0; i < SHOOTDELAY; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        canShoot = true;
    }

    public void DestroyMe(int killer)
    {
        if (isAlive)
        {
            Body.mass = 0.001f;
            isAlive = false;
            canMove = false;
            transform.localScale = new Vector3(0.01f, 0.01f, 0);
            GameMenager.Menager.PlayerWasKilled(killer, PlayerNumber -1);
            Instantiate(Boom, transform.position, transform.rotation);
        }
    }


    public void Mute()
    {
        engine.volume = 0;
        nitro.SetBool("Flies", false);
    }

        public void Reset()
    {
        engine.volume = 0;
        nitro.SetBool("Flies", false);
        transform.localScale = new Vector3(1, 1, 1);
        Body.velocity = new Vector2(0, 0);
        isAlive = true;
        canMove = true;
        Body.mass = 1;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 45 - 90 * (PlayerNumber - 1)));
    }

    public void MoveTo(Vector3 pos)
    {
        transform.position = pos;
    }
}
