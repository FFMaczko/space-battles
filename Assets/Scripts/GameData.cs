﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour {
    public static GameData Data;

    public int NumberOfPlayers;
    private int maxNumberOfPlayers = 4;
    private int minNumberOfPlayers = 2;
    public int TargetScore;
    public Sprite[] PlayerSprite;
    public Color[] PlayerColor;
    public KeyCode[] acelerate = new KeyCode[4];
    public KeyCode[] right = new KeyCode[4];
    public KeyCode[] left = new KeyCode[4];
    public KeyCode[] shoot = new KeyCode[4];

    public int MAXNUMBEROFPLAYERS
    {
        get
        {
            return maxNumberOfPlayers;
        }
    }

    public int MINNUMBEROFPLAYERS
    {
        get
        {
            return minNumberOfPlayers;
        }
    }

    void Awake() {
        if (Data == null)
        {
            Data = this;
            DontDestroyOnLoad(gameObject);
            //ustawiamy sterowanie na domyslne
            acelerate[0] = KeyCode.W;
            acelerate[1] = KeyCode.UpArrow;
            acelerate[2] = KeyCode.T;
            acelerate[3] = KeyCode.I;

            right[0] = KeyCode.D;
            right[1] = KeyCode.RightArrow;
            right[2] = KeyCode.H;
            right[3] = KeyCode.L;

            left[0] = KeyCode.A;
            left[1] = KeyCode.LeftArrow;
            left[2] = KeyCode.F;
            left[3] = KeyCode.J;

            shoot[0] = KeyCode.LeftShift;
            shoot[1] = KeyCode.KeypadEnter;
            shoot[2] = KeyCode.N;
            shoot[3] = KeyCode.Slash;
        }
        else
        {
            Destroy(gameObject);
        }
	}
	
}
