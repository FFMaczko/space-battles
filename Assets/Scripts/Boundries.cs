﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundries : MonoBehaviour {

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.GetComponent<PlayerControler>() != null)
        {
            if (collision.gameObject.transform.position.y >= 15.5f)
            {
                collision.gameObject.transform.position = new Vector3(collision.transform.position.x, -15f, 0);
            }
            if (collision.gameObject.transform.position.y <= - 15.5f)
            {
                collision.gameObject.transform.position = new Vector3(collision.transform.position.x, 15f, 0);
            }
            if (collision.gameObject.transform.position.x >= 27.5)
            {
                collision.gameObject.transform.position = new Vector3(-27.4f, collision.transform.position.y, 0);
            }
            if (collision.gameObject.transform.position.x <= -27.5f)
            {
                collision.gameObject.transform.position = new Vector3(27.4f, collision.transform.position.y, 0);
            }
        }
    }
}
