﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exsplosion : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine("DestroyEffect");
	}
	
    IEnumerator DestroyEffect()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        Destroy(gameObject);
    }
}
