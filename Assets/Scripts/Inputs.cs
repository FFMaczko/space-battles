﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Inputs : MonoBehaviour {
    int buttonID;
    int PlayerNumer = 0;
    const int MAXPLAYERNUMBER = 3;

    // napisy na menu
    public Text player;
    public Text shoot;
    public Text aceleration;
    public Text right;
    public Text left;

    //wyswietlenie poczatkowych danych
    void Start () {
        Refresh();
	}

    // zmiana klawisza
    public void SetButton(int buttonID)
    {
        StopAllCoroutines();
        this.buttonID = buttonID;
        if (buttonID == 0) shoot.text = "Press any key";
        if (buttonID == 1) left.text = "Press any key";
        if (buttonID == 2) right.text = "Press any key";
        if (buttonID == 3) aceleration.text="Press any key";
        StartCoroutine("ButtonSetter");
    }


    public KeyCode KeyPressed()
    {
        int e = System.Enum.GetNames(typeof(KeyCode)).Length;
        for (int i = 0; i < e; i++)
        {
            if (Input.GetKey((KeyCode)i) && ((KeyCode)i)!=KeyCode.None)
            {
                return (KeyCode)i;
            }
        }

        return KeyCode.None;
    }

    IEnumerator ButtonSetter()
    {
        KeyCode newKey=KeyCode.None;
        //czekamy az gracz kliknie klawisz
        while (newKey == KeyCode.None)
        {
            newKey = KeyPressed();
            yield return new WaitForEndOfFrame();

        }

        //przypisujemy klawisz
        if (buttonID == 0) GameData.Data.shoot[PlayerNumer] = newKey;
        if (buttonID == 1) GameData.Data.left[PlayerNumer] = newKey;
        if (buttonID == 2) GameData.Data.right[PlayerNumer] = newKey;
        if (buttonID == 3) GameData.Data.acelerate[PlayerNumer] = newKey;

        //odswierzamy widok menu
        Refresh();
    }



    //zmiana story z klawiszologia
    public void IncreasePlayerNumber()
    {
        if (PlayerNumer <= MAXPLAYERNUMBER - 1)
        {
            PlayerNumer++;
        }
        else
        {
            PlayerNumer = 0;
        }
        Refresh();
    }

    public void DecreasePlayerNumber()
    {
        if (PlayerNumer >= 1)
        {
            PlayerNumer--;
        }
        else
        {
            PlayerNumer = MAXPLAYERNUMBER;
        }
        Refresh();
    }

    private void Refresh()
    {
        player.text = (PlayerNumer+1).ToString();
        shoot.text=GameData.Data.shoot[PlayerNumer].ToString();
        right.text = GameData.Data.right[PlayerNumer].ToString();
        left.text = GameData.Data.left[PlayerNumer].ToString();
        aceleration.text = GameData.Data.acelerate[PlayerNumer].ToString();
    }

}
