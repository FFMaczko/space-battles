﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public GameObject smallExplosion;
    public int bulletOvner;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other != null)
        {
            if (other.transform.GetComponent <Destructible> () != null)
            {
                other.transform.GetComponent<Destructible>().DestroyMe(bulletOvner);
            }
            Instantiate(smallExplosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
