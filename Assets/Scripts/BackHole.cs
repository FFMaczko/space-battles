﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackHole : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other != null)
        {
            if (other.transform.GetComponent<Destructible>() != null)
            {
                other.transform.GetComponent<Destructible>().DestroyMe(-1);
            }
        }
    }

    private void FixedUpdate()
    {
        transform.Rotate(0, 0, -0.3f);
    }
}
