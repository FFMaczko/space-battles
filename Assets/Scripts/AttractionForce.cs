﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractionForce : MonoBehaviour {
    const float GravityAceleration = 6.6f;
    private Transform GravitySource;
    private Vector3 ForceDirection;
    private float ForceValue;
    private Rigidbody2D Body;
    private void Reset()
    {
        GravitySource = GameObject.Find("Planet").transform;
        Body = transform.GetComponent<Rigidbody2D>();

    }

    // Use this for initialization
    void Start () {
        GravitySource = GameObject.Find("Planet").transform;
        Body = transform.GetComponent<Rigidbody2D>();
        ForceValue = Body.mass * GravityAceleration;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        ForceDirection = transform.position - GravitySource.position;
        ForceValue = Body.mass * GravityAceleration / Mathf.Abs(ForceDirection.magnitude);
        Body.AddForce(-ForceDirection * ForceValue);
	}
}
