﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayMenuUI : MonoBehaviour {
    public Text ScoreCounter;
    public Text PlayersCounter;


    //GameMode
    public Text GameModeName;
    public Text Disclaymer;

    private int BaseGameModeNumber=0;
    public string[] GameModeNames;
    public string[] Disclaymers;
    private int MAXBASEGAMEMODENUMBER;

    //Gravity Source
    private int GravitySource=0;
    public Text Planet;
    public Text BlackHole;

    private void Start()
    {
        PlayersCounter.text = GameData.Data.NumberOfPlayers.ToString();
        ScoreCounter.text = GameData.Data.TargetScore.ToString();
        MAXBASEGAMEMODENUMBER = GameModeNames.Length - 1;
        GameModeName.text = GameModeNames[BaseGameModeNumber];
        Disclaymer.text = Disclaymers[BaseGameModeNumber];
        SetGravitySource(GravitySource);
    }

    //GameMode
    public void NextGameMode()
    {
        if (BaseGameModeNumber >= MAXBASEGAMEMODENUMBER)
        {
            BaseGameModeNumber = 0;
        }
        else
        {
            BaseGameModeNumber++;
        }
        GameModeName.text = GameModeNames[BaseGameModeNumber];
        Disclaymer.text = Disclaymers[BaseGameModeNumber];
    }

    public void PreviousGameMode()
    {
        if (BaseGameModeNumber <= 0)
        {
            BaseGameModeNumber = MAXBASEGAMEMODENUMBER;
        }
        else
        {
            BaseGameModeNumber--;
        }
        GameModeName.text = GameModeNames[BaseGameModeNumber];
        Disclaymer.text = Disclaymers[BaseGameModeNumber];
    }

    //GravitySource (1 - BlackHole, 0 - Planet)
    public void SetGravitySource(int i)
    {
        if (i == 0)
        {
            Planet.color = Color.white;
            BlackHole.color = Color.blue;
        }
        else if (i == 1)
        {
            Planet.color = Color.blue;
            BlackHole.color = Color.white;
        }
        GravitySource = i;
    }

    //PlayerNumber
    public void AddPlayer()
    {
        if (GameData.Data.NumberOfPlayers >= GameData.Data.MAXNUMBEROFPLAYERS)
        {
            GameData.Data.NumberOfPlayers = GameData.Data.MINNUMBEROFPLAYERS;
        }
        else
        {
            GameData.Data.NumberOfPlayers++;
        }
        PlayersCounter.text = GameData.Data.NumberOfPlayers.ToString();
    }

    public void SubtractPlayer()
    {
        if (GameData.Data.NumberOfPlayers <= GameData.Data.MINNUMBEROFPLAYERS)
        {
            GameData.Data.NumberOfPlayers = GameData.Data.MAXNUMBEROFPLAYERS;
        }
        else
        {
            GameData.Data.NumberOfPlayers--;
        }
        PlayersCounter.text = GameData.Data.NumberOfPlayers.ToString();
    }

    //TargetScore
    public void AddScore()
    {
        GameData.Data.TargetScore++;
        ScoreCounter.text = GameData.Data.TargetScore.ToString();
    }

    public void SubtractScore()
    {
        if (GameData.Data.TargetScore > 1)
        {
            GameData.Data.TargetScore--;
        }
        ScoreCounter.text = GameData.Data.TargetScore.ToString();
    }

    public void Play()
    {
        SceneManager.LoadScene(2+BaseGameModeNumber*2+GravitySource);
    }
}
