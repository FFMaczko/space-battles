﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour {

    public void PlayMenu()
    {
        SceneManager.LoadScene("PlayMenu");
    }

    public void Controls()
    {
        SceneManager.LoadScene("Inputs");
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
