﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenager : MonoBehaviour {
    //Score Canvas
    public Canvas ScoreCounter;
    private Text[] Points;
    public GameObject PointsCounter;
    const float OFFSET = 50;

    //Message Canvas
    public Canvas MainMessages;
    public Text Message;

    //Pause Canvas
    public Canvas PauseCanvas;
    private bool Paused;

    //EndMenu 
    public Canvas EndCanvas;

    private void Start()
    {
        EndCanvas.enabled=false;
        PauseCanvas.enabled=false;
        StartCoroutine("CheckEscape");
    }

    IEnumerator CheckEscape()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ChangePauseState();
            }
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }

    public bool GetPaused()
    {
        return Paused;
    }

    private void ChangePauseState()
    {
        Paused = !Paused;
        PauseCanvas.enabled = Paused;
        if (Paused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void TurnONEndCanvas()
    {
        EndCanvas.enabled = true;
    }

    public void TurnOffEndCanvas()
    {
        EndCanvas.enabled = false;
    }

    public void TurnONMessages()
    {
        MainMessages.enabled = true;
    }

    public void TurnOffMessages()
    {
        MainMessages.enabled = false;
    }

    public void SetMessage(string message)
    {
        Message.text = message;
    }

    public void SetMessageColor(Color c)
    {
        Message.color=c;
    }

    public void CreateScoreCounter(int numbeOfPlayers)
    {
        Points = new Text[numbeOfPlayers];
        Vector3[] CounterPosition = new Vector3[4];
        CounterPosition[0] = new Vector3(OFFSET,Screen.height-OFFSET,0);
        CounterPosition[1] = new Vector3(Screen.width-OFFSET,Screen.height-OFFSET,0);
        CounterPosition[2] = new Vector3(OFFSET, OFFSET, 0);
        CounterPosition[3] = new Vector3(Screen.width-OFFSET, OFFSET, 0);
        for (int i = 0; i < numbeOfPlayers; i++)
        {
            GameObject go = Instantiate(PointsCounter, CounterPosition[i], ScoreCounter.transform.rotation);
            go.transform.SetParent(ScoreCounter.transform);
            Points[i] = go.transform.GetComponent<Text>();
            Points[i].text = "0";
            Points[i].color = GameData.Data.PlayerColor[i];
        }
    }

    public void SetPoints(int player, int playerPoints)
    {
        Points[player].text = playerPoints.ToString();
    }
}
